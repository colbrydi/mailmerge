# Importing necessary modules
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
import markdown
from IPython.core.display import HTML, display
import pandas as pd
import math
import time
from datetime import datetime
from pathlib import Path, PurePath
import re


def outlookcontacts2df(textstring, deliminator="\n"):
    """
    Convert a string containing Outlook contacts to a pandas DataFrame.

    Parameters:
    - textstring (str): Input string containing Outlook contacts.
    - deliminator (str): Delimiter used to split the input string.

    Returns:
    - pd.DataFrame: DataFrame containing parsed Outlook contacts.
    """
    df = pd.DataFrame()
    mylist = textstring.split(deliminator)
    for row in mylist:
        entry = dict()
        if ',' in row:
            rowlist = row.split(',')
            entry['Last'] = rowlist[0].strip()
            entry['First'] = rowlist[1].split('<')[0].strip()
        else:
            rowlist = row.split(' ')
            entry['First'] = rowlist[0].strip()
            entry['Last'] = rowlist[1].strip()
        entry['Email'] = row.split('<')[1][:-1].strip()
        df = pd.concat([df, pd.DataFrame([entry])], ignore_index=True)
    df = df.drop_duplicates('Email')
    df.sort_values('First', inplace=True)
    return df


def send_check():
    """
    Check if the user wants to send the email and return a boolean value.

    Returns:
    - bool: True if the user wants to send the email, False otherwise.
    """
    sendtest = True
    x = input('Type "SEND" (without the quotes) to send the message:')
    if x == "SEND":
        sendtest = False

    if sendtest:
        print("TESTING Only, Emails will NOT be sent")
    else:
        for i in range(0, 20):
            print("WARNING, DANGER!!!!, IF you continue, Emails will be Sent")
    return sendtest


def init_message(Subject='', Cc='', Bcc='', To='', From='', Message=''):
    """
    Initialize a message template.

    Parameters:
    - Subject (str): Subject of the email.
    - Cc (str): CC field of the email.
    - Bcc (str): BCC field of the email.
    - To (str): Recipient(s) of the email.
    - From (str): Sender of the email.
    - Message (str): Body of the email.

    Returns:
    - dict: Dictionary containing the message template.
    """
    template = dict()
    template['Subject'] = Subject
    template['Cc'] = Cc
    template['Bcc'] = Bcc
    template['To'] = To
    template['From'] = From
    template['Attachments'] = ''
    template['Message'] = Message
    return template


def tabs2df(data):
    """
    Convert tab-delimited data to a pandas DataFrame.

    Parameters:
    - data (str): Tab-delimited data.

    Returns:
    - pd.DataFrame: DataFrame containing parsed data.
    """
    stringList = data.split('\n')
    columns = stringList[0].split('\t')
    stringList = stringList[1:]
    df = pd.DataFrame([data.split('\t') for data in stringList], columns=columns)
    return df


def preview(template, df, test=True):
    """
    Preview the email messages without sending.

    Parameters:
    - template (dict): Message template.
    - df (pd.DataFrame): DataFrame containing email data.
    - test (bool): Whether to perform a test preview without sending.

    Returns:
    - None
    """
    sendmessages(template, df, test=test)


def loopsend(template, df, maxsend=750, start=0, duration=(60*60)+10, server='smtp.egr.msu.edu', test=True):
    """
    Send email messages in a loop to overcome maxsend limits.

    Parameters:
    - template (dict): Message template.
    - df (pd.DataFrame): DataFrame containing email data.
    - maxsend (int): Maximum number of emails to send in each iteration.
    - start (int): Starting index in the DataFrame.
    - duration (int): Time interval between iterations in seconds.
    - server (str): SMTP server address.
    - test (bool): Whether to perform a test run without sending.

    Returns:
    - None
    """
    while start <= len(df):
        end = start + maxsend
        if end > len(df):
            end = len(df)
        print(f"From {start} to {end} of {len(df)}")
        block = df.iloc[start:end, :]
        sendmessages(template, block, server=server, test=test)
        print(f"Just Finished {start}-{end} of {len(df)}, Sleeping for {duration} seconds starting {datetime.now()}")
        if not end > len(df):
            print(f"Sleeping {duration} seconds")
            time.sleep(duration)
        start = end


def sendmail(new_message, server='smtp.egr.msu.edu', test=True):
    """
    Send an email message.

    Parameters:
    - new_message (dict): Dictionary containing email message details.
    - server (str): SMTP server address.
    - test (bool): Whether to perform a test run without sending.

    Returns:
    - None
    """
    new_message['MD_message'] = markdown.markdown(new_message['Message'])

    msg = MIMEMultipart('alternative')
    part1 = MIMEText(new_message['Message'], 'plain')
    part2 = MIMEText(new_message['MD_message'], 'html')
    msg['Subject'] = new_message['Subject']
    msg['From'] = new_message['From']
    msg['To'] = new_message['To']
    msg.attach(part1)
    msg.attach(part2)

    rcpt = new_message['To'].split(",")
    if new_message['Cc']:
        rcpt = new_message['Cc'].split(",") + rcpt
        msg['Cc'] = new_message['Cc']
    if new_message['Bcc']:
        rcpt = new_message['Bcc'].split(",") + rcpt
        msg['Bcc'] = new_message['Bcc']

    if new_message['Attachments'] == '':
        files = []
    else:
        files = new_message['Attachments'].split(',')

    for file in files:
        path = Path(file.strip())
        if path.is_file():
            with open(path, "rb") as f:
                attach = MIMEApplication(f.read(), _subtype="pdf")
                attach.add_header('Content-Disposition', 'attachment', filename=str(PurePath(file).name))
            msg.attach(attach)
        else:
            raise Exception(f'ERROR: File {file} not found!')

    if test:
        print('To:', new_message['To'])
        print('From:', new_message['From'])
        if new_message['Cc']:
            print('CC:', new_message['Cc'])
        if new_message['Bcc']:
            print('BCC:', new_message['Bcc'])
        print('Subject:', new_message['Subject'])
        print('Attachments:', new_message['Attachments'])
        display(HTML(new_message['MD_message']))
    else:
        try:
            # Send the message via engineering SMTP server.
            print('Connecting to SMTP and sending message to', new_message['To'])
            keep_server_open = True
            if not isinstance(server, smtplib.SMTP):
                server = smtplib.SMTP(server)
                keep_server_open = False

            server.sendmail(new_message['From'], rcpt, msg.as_string())
            if not keep_server_open:
                server.quit()

        except smtplib.SMTPException:
            print("Error: unable to send email to", new_message['To'])


def applyTags(template, row):
    """
    Apply values from a pandas series to a template dictionary.

    Parameters:
    - template (dict): Message template.
    - row (pd.Series): Pandas series containing values to be applied to the template.

    Returns:
    - dict: Modified message dictionary with applied values.
    """
    new_message = template.copy()
    for component in new_message:
        for j, tag in enumerate(list(row.index)):
            if f"<<{tag}>>" in new_message[component]:
                new_message[component] = new_message[component].replace(f"<<{tag}>>", f"{row[tag]}")
        if "<<" in new_message[component]:
            tags = re.findall('<<.*>>', new_message[component])
            if tags:
                raise Exception(f"ERROR: tags {tags} still found in {component}")
    return new_message


def sendmessages(template, df, server='smtp.egr.msu.edu', sendtest=""):
    """
    Send email messages using a template and DataFrame.

    Parameters:
    - template (dict): Message template.
    - df (pd.DataFrame): DataFrame containing email data.
    - server (str): SMTP server address.
    - sendtest (str): String for testing purposes.

    Returns:
    - None
    """
    if sendtest == "":
        sendtest = send_check()

    if not sendtest:
        server = smtplib.SMTP(server)
    tags = list(df.columns)  # Using columns as tags
    emailcount = 0
    for index, dfrow in df.iterrows():
        emailcount = emailcount + 1
        new_message = template.copy()
        print("========================================================================")

        for component in new_message:
            for j, tag in enumerate(tags):
                new_message[component] = new_message[component].replace(f'<<{tag}>>', f"{dfrow[tag]}")
            if "<<" in new_message[component]:
                tags = re.findall('<<.*>>', new_message[component])
                raise Exception(f"ERROR: tags {tags} still found in {component}")
        sendmail(new_message, server=server, test=sendtest)
    if not sendtest:
        server.quit()


def makepdf(new_message, filename=''):
    """
    Create a PDF file from an email message.

    Parameters:
    - new_message (dict): Dictionary containing email message details.
    - filename (str): Name of the PDF file to be created.

    Returns:
    - None
    """
    from xhtml2pdf import pisa

    new_message['MD_message'] = markdown.markdown(new_message['Message'])

    # open output file for writing (truncated binary)
    result_FID = open(filename, "w+b")

    # convert HTML to PDF
    pisa_status = pisa.CreatePDF(new_message['MD_message'], dest=result_FID)

    # close output file
    result_FID.close()  # close output file


def makePDFs(template, df, folder='./'):
    """
    Create PDF files from email messages.

    Parameters:
    - template (dict): Message template.
    - df (pd.DataFrame): DataFrame containing email data.
    - folder (str): Folder path to save the PDF files.

    Returns:
    - None
    """
    emailcount = 0
    for index, row in df.iterrows():
        emailcount = emailcount + 1
        new_message = applyTags(template, row)
        print("========================================================================")
        file = f"{folder}{new_message['Filename']}"
        print(file)
        makepdf(new_message, filename=file)
